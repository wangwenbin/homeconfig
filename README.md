# homeconfig

#### 介绍
批量配置用户环境，包括ohmyzsh、autojump和SpaceVim


#### 安装教程

1.  `cd somedir` #somedir可以是任何你觉得合适的目录，比如.config或者.local,甚至是~/,但是建议在clone后`mv homeconfig .homeconfig`以隐藏该文件夹
2.  `git clone https://gitee.com/wangwenbin/homeconfig.git --recursive`
3.  `homeconfig/install.sh`
4.  SpaceVim插件在启动vim/nvim后会由SpaceVim自己自动安装。



#### 参与贡献

1.  Fork 本仓库
2.  新建 `Feat_xxx` 分支
3.  提交代码
4.  新建 Pull Request

