
HOMECONFIG_PATH=$(cd "$(dirname $0)"  && pwd)
echo HOMECONFIG_PATH is $HOMECONFIG_PATH ...

ZSHRC=$HOMECONFIG_PATH/zshrc
echo ZSHRC is $ZSHRC ...
AUTOJUMP=$HOMECONFIG_PATH/third_party/autojump
echo AUTOJUMP is $AUTOJUMP ...
SPACEVIM=$HOMECONFIG_PATH/third_party/SpaceVim
echo SPACEVIM is $SPACEVIM ...
SPACEVIMd=$HOMECONFIG_PATH/SpaceVim.d
echo SPACEVIMd is $SPACEVIMd ...

mkdir -p ~/homeconfig-bak
if [ -e ~/.zshrc ] || [ -L ~/.zshrc ]; then
    mv ~/.zshrc ~/homeconfig-bak/zshrc-bak-$(date --iso-8601=seconds)
fi
ln -s $ZSHRC ~/.zshrc
echo "zshrc linked..."

if [ -e ~/.autojump ] || [ -L ~/.autojump ]; then
    mv ~/.autojump ~/homeconfig-bak/autojump-bak-$(date --iso-8601=seconds)
fi
#ln -s $AUTOJUMP ~/.autojump
echo "autojump install..."
cd $AUTOJUMP
 ./install.py


if [ -e ~/.config/nvim ] || [ -L ~/.config/nvim ]; then
    mv ~/.config/nvim ~/homeconfig-bak/nvim-bak-$(date --iso-8601=seconds)
fi
ln -s $SPACEVIM ~/.config/nvim
if [ -e ~/.vim ] || [ -L ~/.vim ]; then
    mv ~/.vim ~/homeconfig-bak/vim-bak-$(date --iso-8601=seconds)
fi
ln -s $SPACEVIM ~/.vim
echo "SpaceVim linked..."

if [ -e ~/.SpaceVim.d ] || [ -L ~/.SpaceVim.d ]; then
    mv ~/.SpaceVim.d ~/homeconfig-bak/SpaceVim.d-bak-$(date --iso-8601=seconds)
fi
ln -s $SPACEVIMd ~/.SpaceVim.d
echo "SPACEVIM.d linked..."

