#Filename: zshrc 

#Persnal config file,like some Change on $PATH.
if [ -f ~/.profile ];then
	source ~/.profile
fi

###################################################################################
#oh-my-zsh config {{
###################################################################################
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="wvb"
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=$(dirname $(realpath ~/.zshrc))/ozsh_custom

# Path to oh-my-zsh installation.
ZSH=$(dirname $(realpath ~/.zshrc))/third_party/oh-my-zsh

# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
plugins=(
	#git
	golang
	autojump
	zsh-autosuggestions
	zsh-syntax-highlighting
	#incr
)

# CASE_SENSITIVE="true"
# HYPHEN_INSENSITIVE="true" # Case-sensitive completion must be off. _ and - will be interchangeable.

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time
# zstyle ':omz:update' frequency 13 # Uncomment the following line to change how often to auto-update (in days).

# DISABLE_MAGIC_FUNCTIONS="true"
# DISABLE_LS_COLORS="true"
# DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="true"

# You can also set it to another string to have that shown instead of the default red dots.
# COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

source $ZSH/oh-my-zsh.sh

alias zshconfig="vi ~/.zshrc"
##########################################################################################
#end oh-my-zsh config }}
##########################################################################################
